var
  express = require('express');

/**
 * Class implementing HTTP server
 */
class Express {
  /**
   * Create new express application
   * @param {Number} port - http port to listen on
   * @param {Array} routes - array of routes (server API)
   */
  constructor(port, routes) {
    /** @type {Object} */
    this.app = express();

    /** @type {Number} */
    this.port = port;

    /** @type {Array} */
    this.routes = routes;
  }

  /**
   * Setup all routes and power up the server
   */
  init() {
    return new Promise((resolve, reject) => {
      this.use(this.routes);
      this.app.listen(this.port, () => {
        console.log(`App listening on port ${this.port}`);
        resolve();
      });
    });
  }

  /**
   * Setup routes
   * @param {Array} routes - array of route objects
   * @example
   * Express.use([{method: 'get', path: '/', handler: getIndex}, {method: 'get', path: 'users', handler: usersList}]);
   */
  use(routes) {
    routes.forEach(route => this.app[route.method](route.path, route.handler));
  }
}

module.exports = Express;
