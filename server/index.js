/**
 * Created by andr on 2/15/16.
 */
"use strict";


const
  port = 5555;

var
  server = new (require('./express'))(port, require('./router')),
  jexia = require('./services/jexia');

server.init().then(() => {
  let
    JexiaClient = new jexia();
});
