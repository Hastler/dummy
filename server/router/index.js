/**
 * Created by andr on 2/15/16.
 */
module.exports = [
  {
    method: 'get',
    path: '/',
    handler: function(req, res) {
      res.send('Hello, world!');
    }
  }
];
