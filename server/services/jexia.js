/**
 * Created by andr on 2/22/16.
 */
"use strict";

var
  request = require('request-promise'),
  websocket = require('websocket').client;

const
  appId = '3cd28b20-d3e9-11e5-9833-97cb07b78fc4',
  appKey = '803a83a865c0402e1b0c89d3784c6cc2',
  appSecret = '04b7488e81928c25c5b013e0456197b6f3b6c5d6c550907b';


class JexiaClient {
  constructor() {

    this.url = 'http://' + appId + '.app.jexia.com/';
    this.rtc_url = 'http://rtc.jexia.com/rtc/';

    this.client = new websocket();

    this.getToken()
      .then(() => this.getAdvice())
      .then(() => this.connect())
      .then(() => this.subscribe())
      .catch(error => console.log(error));
  }

  getToken() {
    let
      options = {
        method: 'POST',
        url: this.url,
        body: {
          key: appKey,
          secret: appSecret
        },
        json: true
      };

    return request(options).then(data => {
      this.refresh_token = data.refresh_token;
      this.token = data.token;

      console.log('Received token and refresh token', this.token, this.refresh_token);
    })
  }

  getAdvice() {
    let
      url = this.rtc_url + '?message=' + encodeURIComponent(JSON.stringify([{
            channel: '/meta/handshake',
            version: '1.0',
            supportedConnectionTypes: ["websocket", "eventsource","long-polling","cross-origin-long-polling","callback-polling"],
            id: 1
          }])),
      options = {
        method: 'GET',
        url: url,
        qs: {
          jsonp: '__encodeJson__'
        },
        json: true
      };

    return request(options).then(data => {
      let
        __encodeJson__ = function(js) {
          return js;
        },
        result = eval(data);

      this.clientId = result[0].clientId;
      this.reconnectTimeout = result[0].advice.timeout;

      console.log('received client id:', this.clientId);

    });
  }

  connect() {
    return new Promise(resolve => {

      this.client.on('connect', connection => {
        console.log('WS Connected!');
        this.connection = connection;
        this.connection.on('message', this.message);
        this.connection.on('error', error => console.log(error));
        this.connection.on('close', function() {
          console.log('echo-protocol Connection Closed');
        });
        resolve();
      });

      this.client.on('connectFailed', function(error) {
        reject(error.toString())
      });

      console.log('try to connect');
      this.client.connect('ws://rtc.jexia.com/rtc');
    })
  }

  subscribe() {
    if (this.connection.connected) {

      setTimeout(() => {
        this.connection.sendUTF("[]");
      }, 30000);

      this.connection.sendUTF(JSON.stringify([{
        channel: "/meta/connect",
        clientId: this.clientId,
        connectionType: "websocket",
        id: "2"
      }]));

      this.connection.sendUTF(JSON.stringify([{
        channel: "/meta/subscribe",
        clientId: this.clientId,
        subscription: "/" + appId + "/users/" + appKey,
        id: "3",
        ext: {
          token: this.token
        }
      }]))
    }
  }

  message(message) {
    console.log(message);
  }

}

module.exports = JexiaClient;
